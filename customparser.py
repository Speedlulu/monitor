import ast
from configparser import ConfigParser

def list_converter(s):
    return ast.literal_eval(s)

parser = ConfigParser(converters={
    'list': list_converter
})

def create_config_dict(config_o):
    config = dict()
    for section in config_o.sections():
        current_dict = dict()
        current_dict['notify'] = config_o.getboolean(section, 'notify')
        current_dict['who'] = config_o.getlist(section, 'who')
        config[section] = current_dict
    return config
