from __future__ import print_function

import sys
import re
import inspect
from pathlib import Path

from api import MulticraftAPI
from deaths import deaths_regexes
from customparser import parser, create_config_dict
from request_notif import request_notif

URL = 'https://panel.pebblehost.com/api.php'

ID_FILE = '/etc/monitor/id.cfg'

if parser.read(ID_FILE):
    USER = parser['ids']['user']
    SERVER_ID = parser['ids']['server_id']
    API_KEY = parser['ids']['key']
else:
    print('Could not load ids', file=sys.stderr)
    exit(1)

CONFIG_FILE = './events_notif.cfg'
CONFIG = {}

CLIENT = MulticraftAPI(URL, USER, API_KEY)

BASE_REGEX = r'(?P<date>\d{2}\.\d{2}) (?P<time>\d{2}:\d{2}:\d{2})\s'
SERVER_BASE_REGEX = r'\[Server\] Server thread/\S{4} (?P<message>%s)'

PLAYERS = []

class Event():
    def __init__(self, date, time):
        self.date = date
        self.time = time

    def __str__(self):
        return '{} {}'.format(self.date, self.time)

class ServerEvent(Event):
    def __init__(self, server_type, *args):
        self.server_type = server_type
        super().__init__(*args)

    def __str__(self):
        return '{} [Server] {}'.format(
            super().__str__(),
            self.server_type
        )

    @classmethod
    def name(cls):
        return 'ServerEvent'

class DeathEvent(ServerEvent):
    def __init__(self, *args, **kwargs):
        self.player_1 = kwargs.get('player_1')
        self.message = kwargs.get('message')
        super().__init__('Server thread/INFO', *args)

    def __str__(self):
        return '{} {}'.format(
            super().__str__(),
            self.message
        )

    @classmethod
    def name(cls):
        return 'DeathEvent'

class PvpDeathEvent(DeathEvent):
    def __init__(self, *args, **kwargs):
        self.player_2 = kwargs.get('player_2')
        self.using = kwargs.get('using')
        super().__init__(*args, **kwargs)

class ConnectEvent(Event):
    def __init__(self, *args, **kwargs):
        self.who = kwargs.get('who')
        self.ip = kwargs.get('ip')
        super().__init__(*args)

    def __str__(self):
        return '{} [Connect] User {}, IP {}'.format(
            super().__str__(),
            self.who,
            self.ip
        )

    @classmethod
    def name(cls):
        return 'ConnectEvent'

class DisconnectEvent(Event):
    def __init__(self, *args, **kwargs):
        self.who = kwargs.get('who')
        self.reason = kwargs.get('reason')
        super().__init__(*args)

    def __str__(self):
        return '{} [Disconnect] User {} has disconnected, reason: {}'.format(
            super().__str__(),
            self.who,
            self.reason
        )

    @classmethod
    def name(cls):
        return 'DisconnectEvent'

class Player():
    def __init__(self, id, pseudo):
        self.id = id
        self.pseudo = pseudo

    def __str__(self):
        return 'Player[{}]: {}'.format(self.id, self.pseudo)

def get_players():
    players = []

    response = CLIENT('listPlayers', SERVER_ID)
    if not response['success']:
        print('Could not get players', file=sys.stderr)
        return []

    for id, pseudo in response['data']['Players'].items():
        players.append(Player(id, pseudo))

    return players

def guess_pvp_or_not(*args, **kwargs):
    global PLAYERS
    player_2 = kwargs.get('player_2')
    if not PLAYERS:
        PLAYERS = get_players()
    if player_2 and player_2 in [player.pseudo for player in PLAYERS]:
        return PvpDeathEvent(*args, **kwargs)
    return DeathEvent(*args, **kwargs)

REGEX_EVENTS = (
    ([SERVER_BASE_REGEX % regex for regex in deaths_regexes], guess_pvp_or_not),
    (r'\[Connect\] User (?P<who>\S+), IP (?P<ip>\S+)', ConnectEvent),
    (r'\[Disconnect\] User (?P<who>\S+) has disconnected, reason: (?P<reason>\S+)', DisconnectEvent),
)

def get_logs():
    logs = CLIENT('getServerLog', SERVER_ID)
    if not logs['success']:
        print('Could not get logs (%s)' % logs['errors'], file=sys.stderr)
        return []
    logs = [dic['line'] for dic in logs['data']]

    old_logs_file = Path('./old_logs')

    try:
        old_logs = old_logs_file.read_text(encoding='utf-8')
    except FileNotFoundError:
        old_logs = ''

    old_logs = old_logs.split('\n')

    new_logs = []

    for line in reversed(logs):
        if line != old_logs[-1]:
            new_logs.insert(0, line)
        else:
            break

    old_logs_file.write_text('\n'.join(logs))

    return new_logs

def get_events_from_logs(logs):
    list_events = []
    for line in logs:
        m = None
        for regex, event in REGEX_EVENTS:
            if isinstance(regex, list):
                for reg in regex:
                    m = re.fullmatch(BASE_REGEX + reg, line)
                    if m:
                        break
            else:
                m = re.fullmatch(BASE_REGEX + regex, line)
            if m:
                kwargs = m.groupdict()
                date = kwargs.pop('date')
                time = kwargs.pop('time')
                list_events.append(event(date, time, **kwargs))
                break
    return list_events

def need_to_notify(event):
    notify = CONFIG[event.name()]['notify']
    who = CONFIG[event.name()]['who']
    if (notify and
        (
            '*' in who or
            event.__dict__.get('who') in who or
            event.__dict__.get('player_1') in who or
            event.__dict__.get('player_2') in who
        )
    ):
        return True
    return False

def make_default_config():
    default_config = ''
    for event in [name for name, obj in inspect.getmembers(sys.modules[__name__], inspect.isclass) if name.endswith('Event', 1)]:
        default_config += '[{}]\n'.format(event)
        default_config += 'notify = false\n'
        default_config += 'who = \n\n'

    return default_config

def get_config(config_file):
    config_file_fd = Path(config_file)
    if not config_file_fd.exists():
        config_file_fd.write_text(make_default_config())
    parser.clear()
    with config_file_fd.open() as f:
        parser.read_file(f)
    return create_config_dict(parser)

def get_events_to_notify():
    global CONFIG
    if not CONFIG:
        CONFIG = get_config(CONFIG_FILE)
    events_to_notify = []
    events = get_events_from_logs(
        get_logs()
    )

    for event in events:
        if need_to_notify(event):
            events_to_notify.append(event)

    return events_to_notify


def do_notif():
    events = get_events_to_notify()
    for event in events:
        request_notif('MC Frangins', str(event))
    else:
        if len(events):
            print('Notfied {} events'.format(len(events)))
        exit(0)
