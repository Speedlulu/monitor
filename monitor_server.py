#! /usr/bin/python3

import traceback
from monitor import do_notif
try:
    do_notif()
except Exception:
    traceback.print_exc()
    exit(-1)
