#!/usr/bin/python3

import requests
from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_v1_5
from Crypto.Hash import SHA256
from base64 import b64encode
from os.path import expanduser

DEFAULT_KEY = '~/ssh/id_rsa_api'

def make_signature(url, method, key):
    string_to_sign = method.upper() + ' ' + url
    if string_to_sign[-1] == '/' : string_to_sign = string_to_sign[:-1]
    key = RSA.importKey(open(expanduser('~/.ssh/id_rsa_api')).read())
    string_hash = SHA256.new(string_to_sign.encode('utf-8'))
    signature = PKCS1_v1_5.new(key).sign(string_hash)
    return b64encode(signature).decode('utf-8')

def request_notif(title, body, key=DEFAULT_KEY):
    url = 'https://lucasnetwork.fr:444/notif/new'
    try:
        r = requests.post(url, data = {'title' : title, 'body' : body}, headers = {'X-API-Signature' : make_signature(url, 'POST', key)})
    except requests.ConnectionError:
            return False
    return r.ok

if __name__ == '__main__':
    import sys
    import argparse

    parser = argparse.ArgumentParser(description='Send notification request via API')
    parser.add_argument('title', nargs=1, help='Title of notification')
    parser.add_argument('body', nargs=1, help='Body of notification')
    parser.add_argument(
        '-k', '--key',
        help='Key file to encrypt request',
        metavar='key_file',
        default='~/.ssh/id_rsa_api'
    )
    args = parser.parse_args(sys.argv[1:])
    exit(0 if request_notif(sys.argv[1], sys.argv[2], key=args.key) else -1)

